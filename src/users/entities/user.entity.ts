export class User {
  id: number;
  login: string;
  password: string;
  roles: ('admin' | 'user')[];
  gender: 'male' | 'emale';
  age: number;
}
