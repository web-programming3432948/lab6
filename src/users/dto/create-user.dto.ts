export class CreateUserDto {
  login: string;
  password: string;
  roles: ('admin' | 'user')[];
  gender: 'male' | 'emale';
  age: number;
}
